const Bitwise = require('../')
let bit = new Bitwise()

describe('Bitwise Class', () => {
  describe('=> Instancia', () => {
    it('Deberia tener una propiedad "mask"', () => {
      expect(bit).have.property('mask')
    })
    it('"mask" deberia ser de tipo "number"', () => {
      expect(bit.mask).be.a('number')
    })
    it('"mask" deberia ser igual a 255', () => {
      expect(bit.mask).be.equal(255)
    })
    it('Deberia tener una propiedad "length"', () => {
      expect(bit).have.property('length')
    })
    it('"length" deberia ser de tipo "number"', () => {
      expect(bit.length).be.a('number')
    })
    it('"length" deberia ser igual a 8', () => {
      expect(bit.length).be.equal(8)
    })
    it('Deberia tener una propiedad "base"', () => {
      expect(bit).have.property('base')
    })
    it('"base" deberia ser de tipo "string"', () => {
      expect(bit.base).be.a('string')
    })
    it('"base" deberia ser igual a "00000000"', () => {
      expect(bit.base).be.equal('00000000')
    })
  })
  describe('=> toBitString', () => {
    it('Deberia regresar "00000101" al recibir 5', () => {
      expect(bit.toBitString(5)).to.equal('00000101')
    })
    it('Deberia regresar "11111111" al recibir 255', () => {
      expect(bit.toBitString(255)).to.equal('11111111')
    })
    it('Deberia regresar "00000000" al recibir 256', () => {
      expect(bit.toBitString(256)).to.equal('00000000')
    })
    it('Deberia regresar "00000001" al recibir 257', () => {
      expect(bit.toBitString(257)).to.equal('00000001')
    })
  })
  describe('=> Operadores', () => {
    describe('=>> _convert', () => {
      it('Deberia regresar 8 al pasar 8', () => {
        expect(bit._convert(8)).to.equal(8)
      })
      it('Deberia regresar 0 al pasar 256', () => {
        expect(bit._convert(256)).to.equal(0)
      })
      it('Deberia regresar 1 al pasar 257', () => {
        expect(bit._convert(257)).to.equal(1)
      })
    })
    describe('==> not', () => {
      it('Deberia regresar 2 al pasar 5', () => {
        expect(bit.not(5)).to.equal(2)
      })
      it('Deberia regresar 5 al pasar -6', () => {
        expect(bit.not(-6)).to.equal(5)
      })
      it('Deberia regresar 1 al pasar 254', () => {
        expect(bit.not(254)).to.equal(1)
      })
      it('Deberia regresar 0 al pasar 255', () => {
        expect(bit.not(255)).to.equal(0)
      })
      it('Deberia regresar 255 al pasar 0', () => {
        expect(bit.not(0)).to.equal(255)
      })
      it('Deberia regresar 255 al pasar 256', () => {
        expect(bit.not(256)).to.equal(255)
      })
      it('Deberia regresar 254 al pasar 257', () => {
        expect(bit.not(257)).to.equal(254)
      })
    })
    describe('=>> and', () => {
      it('Deberia regresar 5 al pasar (5,7)', () => {
        expect(bit.and(5,7)).to.equal(5)
      })
      it('Deberia regresar 5 al pasar (7,5)', () => {
        expect(bit.and(7,5)).to.equal(5)
      })
      it('Deberia regresar 0 al pasar (0,X)', () => {
        expect(bit.and(0,5)).to.equal(0)
      })
      it('Deberia regresar 0 al pasar (5,2)', () => {
        expect(bit.and(5,2)).to.equal(0)
      })
      it('Deberia regresar 7 al pasar (7,7)', () => {
        expect(bit.and(7,7)).to.equal(7)
      })
    })
    describe('=>> or', () => {
      it('Deberia regresar 7 al pasar (5,7)', () => {
        expect(bit.or(5,7)).to.equal(7)
      })
      it('Deberia regresar 7 al pasar (7,5)', () => {
        expect(bit.or(7,5)).to.equal(7)
      })
      it('Deberia regresar 6 al pasar (0,6)', () => {
        expect(bit.or(0,6)).to.equal(6)
      })
      it('Deberia regresar 7 al pasar (5,2)', () => {
        expect(bit.or(5,2)).to.equal(7)
      })
      it('Deberia regresar 7 al pasar (7,7)', () => {
        expect(bit.or(7,7)).to.equal(7)
      })
    })
    describe('=>> xor', () => {
      it('Deberia regresar 2 al pasar (5,7)', () => {
        expect(bit.xor(5,7)).to.equal(2)
      })
      it('Deberia regresar 2 al pasar (7,5)', () => {
        expect(bit.xor(7,5)).to.equal(2)
      })
      it('Deberia regresar 6 al pasar (0,6)', () => {
        expect(bit.xor(0,6)).to.equal(6)
      })
      it('Deberia regresar 7 al pasar (5,2)', () => {
        expect(bit.xor(5,2)).to.equal(7)
      })
      it('Deberia regresar 0 al pasar (7,7)', () => {
        expect(bit.xor(7,7)).to.equal(0)
      })
    })
    describe('=>> left (<<)', () => {
      it('Deberia regresar 8 al pasar (2,2)',() => {
        expect(bit.left(2,2)).to.equal(8)
      })
      it('Deberia regresar 252 al pasar (255,2)',() => {
        expect(bit.left(255,2)).to.equal(252)
      })
    })
    describe('=>> right (>>)', () => {
      it('Deberia regresar 0 al pasar (2,2)',() => {
        expect(bit.right(2,2)).to.equal(0)
      })
      it('Deberia regresar 2 al pasar (8,2)',() => {
        expect(bit.right(8,2)).to.equal(2)
      })
      it('Deberia regresar 63 al pasar (255,2)',() => {
        expect(bit.right(255,2)).to.equal(63)
      })
    })
  })
})
