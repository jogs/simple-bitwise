module.exports = class Bitwise {
  constructor (length = 8) {
    length = Math.abs(length)
    if (length > 512) {
      this.length = 512
    } else if (length < 1) {
      this.length = 1
    } else {
      this.length = length
    }
    this.mask = Math.pow(2, length) - 1
    this.base = ''
    for (let i = 0; i < this.length; i++) {
      this.base += '0'
    }
  }
  _convert (a = 0, mask = this.mask) {
    return Number(a) & mask
  }
  _getMask (a = 0) {
    return Math.pow(2, a.toString(2).length) - 1
  }
  and (a, b = 0) {
    return this._convert(a & b)
  }
  or (a, b = 0) {
    return this._convert(a | b)
  }
  xor (a, b = 0) {
    return this._convert(a ^ b)
  }
  not (a = 0) {
    return (a > this.mask || a <= 0) ? this._convert(~a) : this._convert(~a, this._getMask(a))
  }
  left (a, n = 0) {
    return this._convert(a << n)
  }
  right (a, n = 0) {
    return this._convert(a >> n)
  }
  zeroFill (a, n = 0) {
    return this._convert(a) >>> n
  }
  toBitString (a = 0) {
    return (a === 0) ? this.base : (this.base + this._convert(a).toString(2)).slice(-this.base.length)
  }
}
