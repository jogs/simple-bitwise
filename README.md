# Simple Bitwise

Esta es una pequeña clase para hacer operaciones entre bits,
preferentemente positivos y con una logitud menor a 128

## Uso

```javascript
const Bitwise = require('simple-bitwise')
// Bitwise class

let bit = new Bitwise(8)
// bit works with 8 bits

console.log(bit.toBitString(15))
// print => '00001111'

console.log(bit.toBitString(256))
// print => '00000000' (no print overflow)

bit.not(5) // return 2, (~5 return -6)
bit.and(5,7) // return 5
bit.or(5,7) // return 7
bit.xor(5,7) // return 2
bit.left(255,2) // return 253
bit.right(255,2) // return 63
```

## Test

``` bash
make test
```
